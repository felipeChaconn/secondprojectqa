﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.UITest;

namespace UITestRecetas.App
{
    internal class RegisterPage : BaseApp
    {
        public RegisterPage(Platform platform) : base(platform)
        {
        }

        internal bool CreateRecipe(string title, string description, string serves, string cookTime, string ingredient1, string ingredient2, string step1, string step2)
        {
            TouchById("createTabFragment");
            WaitForChargeScreen("createTabCoordinatorLayout");
            TouchById("createRecipeButton");
            //Se ingresa al Recipe Creation
            WaitForChargeScreen("doneButton");
            Write("recipeTitleText", title);
            Write("storyEditText", description);
            Write("metaDataServingText", serves);
            Write("metaDataCookingTimeText", cookTime);
            Write("ingredientEditText", ingredient1);
            ScrollDown("addStepButton", "ingredientEditText");
            Write("stepDescriptionTextView", step1);
            Write("ingredientEditText", ingredient1);
            TouchById("ingredientEditText");
            TouchById("stepDescriptionTextView");

            TouchById("doneButton");

            bool res = WaitForChargeScreen("createTabCoordinatorLayout");

            return res;
        }
    }
}
