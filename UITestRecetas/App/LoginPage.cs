﻿using NUnit.Framework.Constraints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.UITest;

namespace UITestRecetas.App
{
    internal class LoginPage : BaseApp
    {
        public LoginPage(Platform platform) : base(platform)
        {

        }

        internal bool login(string username, string password)
        {
            TouchById("continueToRegionSelectionButton");
            TouchById("regionSelectionList");
            TouchById("loginWithEmailButton");
            Write("emailEditText", username);
            Write("loginPasswordEditText", password);
            TouchById("emailLoginButton");
            bool isCharged = WaitForChargeScreen("homeBase");
            if (isCharged)
            {
                TouchById("actionButton");
            }
            app.Print.Tree(); // Le da un formato de todos los ids 
            return isCharged;
        }
    }
}
