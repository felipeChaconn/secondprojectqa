﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using UITestRecetas.App;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace UITestRecetas.Test
{
    [TestFixture(Platform.Android)]
    internal class RegisterTest : BaseTest
    {
        private RegisterPage RegisterPage;
        public RegisterTest(Platform platform) : base(platform)
        {
            this.RegisterPage = new RegisterPage(platform);
        }

        [Test]
        public void RegisterRecipeTest()
        {
            LoginTest();

            //Arrange
            string Title = "Pollo horneado",
            Description = "Pollo que se come en el horno.",
            Serves = "4 porciones",
            CookTime = "1 hora",
            Ingredient1 = "Pollo",
            Ingredient2 = "Salsa BBQ",
            Step1 = "Inserte el pollo en el horno",
            Step2 = "Coma el pollo en el horno.";
            //Act

            bool result = this.RegisterPage.CreateRecipe(Title, Description, Serves, CookTime, Ingredient1, Ingredient2, Step1, Step2);

            //Assert

            Assert.IsTrue(result, "No se creo el registro.");
        }

        [Test]
        public void RegisterRecipeNoDataTest()
        {
            LoginTest();

            //Arrange
            string Title = null,
            Description = null,
            Serves = null,
            CookTime = null,
            Ingredient1 = null,
            Ingredient2 = null,
            Step1 = null,
            Step2 = null;
            //Act

            bool result = this.RegisterPage.CreateRecipe(Title, Description, Serves, CookTime, Ingredient1, Ingredient2, Step1, Step2);

            //Assert

            Assert.IsFalse(result, "No se creo el registro.");
        }


        [Test]
        public void RegisterRecipeDataIncorrectTest()
        {
            LoginTest();

            //Arrange
            string Title = "QQQQ",
            Description = "Pollo que se come en el horno.",
            Serves = "4 porciones",
            CookTime = "1 hora",
            Ingredient1 = "Pollo",
            Ingredient2 = "Salsa BBQ",
            Step1 = "Inserte el pollo en el horno",
            Step2 = "Coma el pollo en el horno.";
            //Act

            bool result = this.RegisterPage.CreateRecipe(Title, Description, Serves, CookTime, Ingredient1, Ingredient2, Step1, Step2);

            //Assert

            Assert.IsFalse(result, "No se creo el registro.");
        }
    }
}
