﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using UITestRecetas.App;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace UITestRecetas.Test
{
    [TestFixture(Platform.Android)]
    internal class LoginTest : BaseTest
    {
        
        public LoginTest(Platform platform) : base(platform)
        {

        }


        [Test]
        public void LoginNoPasswordTest()
        {
            //Arrange
            string username = "proyectoqa20@gmail.com";
            string password = null;
            //Act 
            bool result = LoginPage.login(username, password);
            //Assert
            Assert.IsFalse(result, "Se inició sesión sin contraseña.");
        }

        [Test]
        public void LoginIncorrectCredentialTest()
        {
            //Arrange
            string username = "proyectoqa20@gmail.com";
            string password = "dfo12345*";
            //Act 
            bool result = LoginPage.login(username, password);
            //Assert
            Assert.IsFalse(result, "Se inició sesión con credenciales incorrectas.");
        }
    }
}
