﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace UITestRecetas
{
    public class AppInitializer
    {
        //Denner
        private const string path = @"C:\Users\denne\Documents\Universidad\Cuatri 11\QA\Proyecto 2\secondprojectqa\UITestRecetas\UITestRecetas\com.mufumbo.android.recipe.search_2.166.1.0-android-30216610_minAPI21(nodpi)_apkmirror.com.apk";
        //private const string path = "../../com.mufumbo.android.recipe.search_2.166.1.0-android-30216610_minAPI21(nodpi)_apkmirror.com.apk";
        public static IApp StartApp(Platform platform)
        {
            
            return ConfigureApp.Android
                .ApkFile(path)
                .StartApp();
        }
    }
}