﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using UITestRecetas.App;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace UITestRecetas
{
    [TestFixture(Platform.Android)]
    class BaseTest
    {
        protected readonly LoginPage LoginPage;
        protected IApp app;
        protected Platform platform;

        public BaseTest(Platform platform)
        {
            this.platform = platform;
            //app = AppInitializer.StartApp(platform);
            LoginPage = new LoginPage(platform);
        }

        [Test]
        public void LoginTest()
        {
            //Arrange
            string username = "proyectoqa20@gmail.com";
            string password = "qa12345*";
            //Act 
            bool result = LoginPage.login(username, password);
            //Assert
            Assert.IsTrue(result, "No se pudo iniciar sesión");
        }
    }
}
