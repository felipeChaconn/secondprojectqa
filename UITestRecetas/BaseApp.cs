﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace UITestRecetas
{
    [TestFixture(Platform.Android)]
    class BaseApp
    {
        protected IApp app;
        protected Platform platform;

        public BaseApp(Platform platform)
        {
            this.platform = platform;
            app = AppInitializer.StartApp(platform);
        }

        /// <summary>
        /// Método para tocar algo por su id
        /// </summary>
        /// <param name="id">ID de referencia de lo que deseas tocar</param>
        protected void TouchById(string id)
        {
            app.Tap(id);
        }

        /// <summary>
        /// Método para escribir en una caja de texto
        /// </summary>
        /// <param name="id">ID de la caja de texto</param>
        /// <param name="text">Texto que se desea escribir</param>
        protected void Write(string id, string text)
        {
            app.Query(e => e.Marked(id).Invoke("setText", text));
        }

        /// <summary>
        /// Método para esperar la carga de una pantalla
        /// </summary>
        /// <param name="screenId">ID del elemento referenciador de la pantalla</param>
        protected bool WaitForChargeScreen(string screenId)
        {
            try
            {
                app.WaitForElement(x => x.Marked(screenId));
                
            }
            catch (TimeoutException e)
            {
                return false;
            }
            catch(Exception e)
            {
                return false;
            }
            return true;
        }

        protected void ScrollDown(string To, string selector)
        {
            app.ScrollDownTo(To, selector, ScrollStrategy.Gesture, swipeSpeed: 2000, timeout: TimeSpan.FromSeconds(60));
        }


    }
}
